using System;

public struct Point
{
    public double X;
    public double Y;

    public Point(double x, double y):this()
    {
        X = x;
        Y = y;
    }
}

public class Rectangle
{
    public Point LeftUpPoint;
    public double Width { get; private set; }
    public double Height { get; private set; }

    //default constructor
    public Rectangle()
    {
        LeftUpPoint.X = 0;
        LeftUpPoint.Y = 0;
        Width = 0;
        Height = 0;
    }

    //constructor with point, width and height
    public Rectangle(Point firstPoint, double width, double height)
    {
        LeftUpPoint = firstPoint;
        Width = width;
        Height = height;
    }

    //constructor with point's parameters
    public Rectangle(double x, double y, double width, double height) : this(new Point(x, y), width, height) {}

    //moving rectangle on (x; y)
    public void Move(double x, double y)
    {
        LeftUpPoint.X += x;
        LeftUpPoint.Y += y;
    }

    //change size from center
    public void ChangeSize(double changeWidth, double changeHeight)
    {
        LeftUpPoint.X -= changeWidth/2;
        LeftUpPoint.Y -= changeHeight/2;
        Width += changeWidth;
        Height += changeHeight;
    }

    //returns rectangle that is the intersection of parameter rectangles or the empty rectangle
    public static Rectangle Intersect(Rectangle firstRectangle, Rectangle secondRectangle)
    {
        double x1 = Math.Max(firstRectangle.LeftUpPoint.X, secondRectangle.LeftUpPoint.X);
        double x2 = Math.Min(firstRectangle.LeftUpPoint.X + firstRectangle.Width,
            secondRectangle.LeftUpPoint.X + secondRectangle.Width);
        double y1 = Math.Min(firstRectangle.LeftUpPoint.Y, secondRectangle.LeftUpPoint.Y);
        double y2 = Math.Max(firstRectangle.LeftUpPoint.Y - firstRectangle.Height,
            secondRectangle.LeftUpPoint.Y - secondRectangle.Height);

        if (x2 >= x1 && y2 <= y1)
        {
            return new Rectangle(x1, y1, x2 - x1, y1-y2);
        }
        return new Rectangle();
    }

    //returns rectangle that is the union of parameter rectangles
    public static Rectangle Union (Rectangle firstRectangle, Rectangle secondRectangle)
    {
        double x1 = Math.Min(firstRectangle.LeftUpPoint.X, secondRectangle.LeftUpPoint.X);
        double x2 = Math.Max(firstRectangle.LeftUpPoint.X + firstRectangle.Width, secondRectangle.LeftUpPoint.X + secondRectangle.Width);
        double y1 = Math.Max(firstRectangle.LeftUpPoint.Y, secondRectangle.LeftUpPoint.Y);
        double y2 = Math.Min(firstRectangle.LeftUpPoint.Y - firstRectangle.Height, secondRectangle.LeftUpPoint.Y - secondRectangle.Height);

        return new Rectangle(x1, y1, x2 - x1, y1-y2);
    }

    public override string ToString()
    {
        return "Coordinates of point = ( "+LeftUpPoint.X+", "+LeftUpPoint.Y+")\tWidth = "+Width+"\tHeight = "+Height;
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        //declaring rectangles and outputting them
        Rectangle firstRectangle=new Rectangle(10, 5, 5, 9);
        Rectangle secondRectangle = new Rectangle(10, 5, 5, 9);

        Console.WriteLine("First rectangle:\n\t"+firstRectangle);
        Console.WriteLine("Second rectangle:\n\t" + secondRectangle);

        //results of moving
        firstRectangle.Move(5, 3);
        secondRectangle.Move(-3,-1);
        Console.WriteLine("\nOperation\tRectangle 1\t Rectangle 2\n------------------------------------------------\nMove\t\t(5,3)\t\t(-3,-1)");
        Console.WriteLine("\nResults:");
        Console.WriteLine("First rectangle:\n\t" + firstRectangle);
        Console.WriteLine("Second rectangle:\n\t" + secondRectangle);

        //results of changing size
        firstRectangle.ChangeSize(9,18);
        secondRectangle.ChangeSize(-2,-2);
        Console.WriteLine("------------------------------------------------\nChange size\t\t(9,18)\t\t(-2,-2)");
        Console.WriteLine("\nResults:");
        Console.WriteLine("First rectangle:\n\t" + firstRectangle);
        Console.WriteLine("Second rectangle:\n\t" + secondRectangle);
       
        //results of intersaction
        Rectangle intersectedRectangle=Rectangle.Intersect(firstRectangle,secondRectangle);
        Console.WriteLine("------------------------------------------------\nIntersection\t\t-\t\t-");
        Console.WriteLine("\nResults:");
        Console.WriteLine("Intersected rectangle:\n\t" + intersectedRectangle);

        //results of union
        Rectangle unionRectangle = Rectangle.Union(firstRectangle, secondRectangle);
        Console.WriteLine("------------------------------------------------\nUnion\t\t-\t\t-");
        Console.WriteLine("\nResults:");
        Console.WriteLine("Union rectangle:\n\t" + unionRectangle);

        Console.Read();

    }
}